import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
// import 'package:flutter_html_view/flutter_html_view.dart';

class DescriptionTextWiget extends StatefulWidget {

  final String text;

  DescriptionTextWiget({@required this.text, Key key}) : super(key: key);

  _DescriptionTextWigetState createState() => _DescriptionTextWigetState();
}

class _DescriptionTextWigetState extends State<DescriptionTextWiget> {
  
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 150) {
      firstHalf = widget.text.substring(0, 150);
      secondHalf = widget.text.substring(150, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: secondHalf.isEmpty
        ? new Text(firstHalf)
        : new Column(
            children: <Widget>[
              // new HtmlView(data: flag ? (firstHalf + "...") : (firstHalf + secondHalf),),
              new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
              new InkWell(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Text(
                      flag ? "show more" : "show less",
                      style: new TextStyle(color: Colors.blue),
                    ),
                  ],
                ),
                onTap: () {
                  setState(() {
                    flag = !flag;
                  });
                },
              ),
            ],
          ),
    );
  }
}