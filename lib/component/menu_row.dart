import 'package:flutter/material.dart';
import 'label_below_icon.dart';

class MenuRow extends StatelessWidget {
  final firstLabel;
  final IconData firstIcon;
  final firstIconCircleColor;
  final firstOnPressed;
  final secondLabel;
  final IconData secondIcon;
  final secondIconCircleColor;
  final secondOnPressed;
  final thirdLabel;
  final IconData thirdIcon;
  final thirdIconCircleColor;
  final thirdOnPressed;
  final fourthLabel;
  final IconData fourthIcon;
  final fourthIconCircleColor;
  final fourthOnPressed;

  const MenuRow(
      {Key key,
      this.firstLabel,
      this.firstIcon,
      this.firstIconCircleColor,
      this.firstOnPressed,
      this.secondLabel,
      this.secondIcon,
      this.secondIconCircleColor,
      this.secondOnPressed,
      this.thirdLabel,
      this.thirdIcon,
      this.thirdIconCircleColor,
      this.thirdOnPressed,
      this.fourthLabel,
      this.fourthIcon,
      this.fourthIconCircleColor,
      this.fourthOnPressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          LabelBelowIcon(
            icon: firstIcon,
            label: firstLabel,
            circleColor: firstIconCircleColor,
            onPressed: firstOnPressed,
          ),
          LabelBelowIcon(
            icon: secondIcon,
            label: secondLabel,
            circleColor: secondIconCircleColor,
            onPressed: secondOnPressed,
          ),
          LabelBelowIcon(
            icon: thirdIcon,
            label: thirdLabel,
            circleColor: thirdIconCircleColor,
            onPressed: thirdOnPressed,
          ),
          LabelBelowIcon(
            icon: fourthIcon,
            label: fourthLabel,
            circleColor: fourthIconCircleColor,
            onPressed: fourthOnPressed,
          ),
        ],
      ),
    );
  }
}
