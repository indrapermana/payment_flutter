import 'package:flutter/material.dart';

class PaketData extends StatefulWidget {
  _PaketDataState createState() => _PaketDataState();
}

class _PaketDataState extends State<PaketData> {

  TextEditingController controllerNomor = new TextEditingController();

  List _paket = ["" ,"500 MB", "1 GB", "2 GB", "3 GB", "6 GB", "11 GB", "21 GB", "31 GB", "36 GB", "46 GB", "56 GB", "66 GB"];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentPaket;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentPaket = _dropDownMenuItems[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for(String paket in _paket){
      items.add(new DropdownMenuItem(
        value: paket,
        child: new Text(paket),
      ));
    }
    return items;
  }

  void changeDropDownItem(String seletedPaket){
    setState(() {
      _currentPaket = seletedPaket;
    });
  }

  void bayar(){

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Paket Data"),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                TextField(
                  controller: controllerNomor,
                  decoration: new InputDecoration(
                    hintText: "Nomor Meter", 
                    labelText: "Nomor Meter",
                    icon: Icon(Icons.contacts)
                  ),
                  keyboardType: TextInputType.number,
                ),
                SizedBox(
                  height: 10,
                ),
                FormField(
                  builder: (FormFieldState state){
                    return InputDecorator(
                      decoration: InputDecoration(
                        icon: Icon(Icons.autorenew),
                        labelText: "Paket",
                      ),
                      isEmpty: _currentPaket == '',
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: _currentPaket,
                          items: _dropDownMenuItems,
                          onChanged: changeDropDownItem,
                          isDense: true,
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: 250,
                ),
                RaisedButton(
                  child: new Text("Bayar", style: TextStyle(color: Colors.teal, fontWeight: FontWeight.bold, fontSize: 18)),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Colors.teal,
                      width: 0.8,
                      style: BorderStyle.solid
                    ),
                    borderRadius: BorderRadius.circular(20)
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 70),
                  onPressed: () {
                    bayar();
                    // Navigator.pop(context);
                  },
                ) 
              ],
            ),
          ],
        )
      ),
    );
  }
}