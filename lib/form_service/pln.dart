import 'package:flutter/material.dart';

import 'pln_prabayar.dart';
import 'pln_pascabayar.dart';

class Pln extends StatefulWidget {
  Pln({Key key}) : super(key: key);

  _PlnState createState() => _PlnState();
}

class _PlnState extends State<Pln> with SingleTickerProviderStateMixin {

  TabController controllerPln;

  @override
  void initState() { 
    controllerPln = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() { 
    controllerPln.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pembayaran PLN"),
        bottom: TabBar(
          controller: controllerPln,
          tabs: <Widget>[
            Tab(text: "Prabayar",),
            Tab(text: "Pascabayar",)
          ],
        ),
      ),
      body: TabBarView(
        controller: controllerPln,
        children: <Widget>[
          PlnPrabayar(),
          PlnPascabayar()
        ],
      ),
    );
  }
}