import 'package:flutter/material.dart';

class PlnPascabayar extends StatefulWidget {
  PlnPascabayar({Key key}) : super(key: key);

  _PlnPascabayarState createState() => _PlnPascabayarState();
}

class _PlnPascabayarState extends State<PlnPascabayar> {

  TextEditingController controllerIdPlanggan = new TextEditingController();

  void bayar(){

  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              TextField(
                controller: controllerIdPlanggan,
                decoration: new InputDecoration(
                  hintText: "ID Pelanggan", 
                  labelText: "ID Pelanggan",
                  icon: Icon(Icons.contacts),
                ),
                keyboardType: TextInputType.number,
              ),
              RaisedButton(
                child: new Text("Bayar"),
                color: Colors.blueAccent,
                textColor: Colors.white,
                onPressed: () {
                  bayar();
                  // Navigator.pop(context);
                },
              )
            ],
          )
        ],
      ),
    );
  }
}