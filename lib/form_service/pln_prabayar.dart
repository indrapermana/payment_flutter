import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PlnPrabayar extends StatefulWidget {
  @override
  _PlnPrabayarState createState() => _PlnPrabayarState();
}

class _PlnPrabayarState extends State<PlnPrabayar> {

  TextEditingController controllerNomor = new TextEditingController();

  List _nominal = ["" ,"Rp20.000", "Rp50.000", "Rp100.000", "Rp200.000", "Rp500.000", "Rp1.000.000", "Rp5.000.000"];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentNominal;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentNominal = _dropDownMenuItems[0].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for(String nominal in _nominal){
      items.add(new DropdownMenuItem(
        value: nominal,
        child: new Text(nominal),
      ));
    }
    return items;
  }

  void changeDropDownItem(String seletedNominal){
    setState(() {
      _currentNominal = seletedNominal;
    });
  }

  void bayar(){

  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              TextField(
                controller: controllerNomor,
                decoration: new InputDecoration(
                  hintText: "Nomor Meter", 
                  labelText: "Nomor Meter",
                  icon: Icon(Icons.contacts)
                ),
                keyboardType: TextInputType.number,
              ),
              SizedBox(
                height: 10,
              ),
              FormField(
                builder: (FormFieldState state){
                  return InputDecorator(
                    decoration: InputDecoration(
                      icon: Icon(Icons.autorenew),
                      labelText: "Nominal",
                    ),
                    isEmpty: _currentNominal == '',
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton(
                        value: _currentNominal,
                        items: _dropDownMenuItems,
                        onChanged: changeDropDownItem,
                        isDense: true,
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 250,
              ),
              RaisedButton(
                child: new Text("Bayar", style: TextStyle(color: Colors.teal, fontWeight: FontWeight.bold, fontSize: 18)),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Colors.teal,
                    width: 0.8,
                    style: BorderStyle.solid
                  ),
                  borderRadius: BorderRadius.circular(20)
                ),
                padding: EdgeInsets.symmetric(horizontal: 70),
                onPressed: () {
                  bayar();
                  // Navigator.pop(context);
                },
              )
            ],
          )
        ],
      ),
    );
  }
}