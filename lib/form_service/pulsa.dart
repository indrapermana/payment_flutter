import 'package:flutter/material.dart';

import 'pulsa_prabayar.dart';
import 'pulsa_pascabayar.dart';

class Pulsa extends StatefulWidget {
  Pulsa({Key key}) : super(key: key);

  _PulsaState createState() => _PulsaState();
}

class _PulsaState extends State<Pulsa> with SingleTickerProviderStateMixin {

  TabController controllerPulsa;

  @override
  void initState() { 
    controllerPulsa = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() { 
    controllerPulsa.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pulsa"),
        bottom: TabBar(
          controller: controllerPulsa,
          tabs: <Widget>[
            Tab(text: "Prabayar",),
            Tab(text: "Pascabayar",)
          ],
        ),
      ),
      body: TabBarView(
        controller: controllerPulsa,
        children: <Widget>[
          PulsaPrabayar(),
          PulsaPascabayar()
        ],
      ),
    );
  }
}