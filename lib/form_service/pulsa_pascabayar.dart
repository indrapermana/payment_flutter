import 'package:flutter/material.dart';

class PulsaPascabayar extends StatefulWidget {
  _PulsaPascabayarState createState() => _PulsaPascabayarState();
}

class _PulsaPascabayarState extends State<PulsaPascabayar> {

  TextEditingController controllerNomor = new TextEditingController();

  void bayar(){

  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              TextField(
                controller: controllerNomor,
                decoration: new InputDecoration(
                  hintText: "Nomor Hp", 
                  labelText: "Nomor Hp",
                  icon: Icon(Icons.contacts)
                ),
                keyboardType: TextInputType.number,
              ),
              SizedBox(
                height: 330,
              ),
              RaisedButton(
                child: new Text("Bayar", style: TextStyle(color: Colors.teal, fontWeight: FontWeight.bold, fontSize: 18)),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Colors.teal,
                    width: 0.8,
                    style: BorderStyle.solid
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.symmetric(horizontal: 70),
                onPressed: () {
                  bayar();
                  // Navigator.pop(context);
                },
              )
            ],
          )
        ],
      ),
    );
  }
}