import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Detail extends StatelessWidget {
  const Detail({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double cWidth = MediaQuery.of(context).size.width*0.7;
    return Scaffold(
      appBar: AppBar(
        title: Text("Mobi"),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.check_circle, color: Colors.green,),
                  Text("Berhasil", style: TextStyle(color: Colors.green, fontSize: 18, fontWeight: FontWeight.bold))
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Center(
                child: Text("25 Jun 2019, 13:21"),
              ),
            ),
            Container(
              padding: EdgeInsets.all(30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Dari", style: TextStyle(color: Colors.black54, fontSize: 12),),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.account_circle, size: 40, color: Colors.grey,),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        width: cWidth,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("MUHAMMAD INDRA PERMANA", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),),
                            Text("MOBI - 087887772233", style: TextStyle(fontSize: 14),)
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("Penerima", style: TextStyle(color: Colors.black54, fontSize: 12),),
                  Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.grey,
                        child: Icon(
                          Icons.account_balance, 
                          size: 30, 
                          color: Colors.white,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        width: cWidth,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("BANK BTPN - MUHAMMAD INDRA PERMANA", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500), textAlign: TextAlign.left,),
                            // Text("MUHAMMAD INDRA PERMANA", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500), textAlign: TextAlign.left,)
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text("Pesan", style: TextStyle(color: Colors.black54, fontSize: 12),),
                  Container(
                    padding: EdgeInsets.only(bottom: 10, top: 5),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey)),
                    ),
                    child: Text("-", style: TextStyle(color: Colors.black),),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text("No. Referensi", style: TextStyle(color: Colors.black54, fontSize: 12),),
                  Container(
                    padding: EdgeInsets.only(bottom: 10, top: 5),
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey)),
                    ),
                    child: Text("e6jnkj2kjbkj08nkjbn739nkjnasic83n2832n78n4238nij82", style: TextStyle(color: Colors.black),),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text("Sumber Dana", style: TextStyle(color: Colors.black54, fontSize: 12),),
                  Container(
                    padding: EdgeInsets.only(bottom: 10, top: 5),
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Text("MOBI Cash", style: TextStyle(color: Colors.black, fontSize: 14),),
                        ),
                        Container(
                          child: Text("-Rp500.000", style: TextStyle(color: Colors.red[400], fontSize: 14),),
                        ),
                      ],
                    ) ,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}