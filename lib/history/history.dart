import 'package:flutter/material.dart';

import 'detail.dart';

class History extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.black12,
          ),
          child: Text("25 JUN 2019", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 12),),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: InkWell(
            child: ListTile(
              title: Text("Indra", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
              contentPadding: EdgeInsets.only(top: 1),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text("Pembayaran", style: TextStyle(color: Colors.grey, fontSize: 14),),
                  ),
                  Container(
                    child: Text("-Rp500.000", style: TextStyle(color: Colors.red[400], fontSize: 14),),
                  ),
                ],
              ),
            ),
            onTap: () {
              Navigator.push(
                context, 
                MaterialPageRoute(
                  builder: (BuildContext context){
                    return Detail();
                  }
                )
              );
            },
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Sky Parking", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Pembayaran", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text("-Rp4.500", style: TextStyle(color: Colors.red[400], fontSize: 14),),
                      Text("+Point 1.200", style: TextStyle(color: Colors.blue[400], fontSize: 14),)
                    ],
                  )
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Transfer from TOP UP", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Incoming Transfer", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Text("+Rp1.000.000", style: TextStyle(color: Colors.green[600], fontSize: 14),),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.black12,
          ),
          child: Text("25 JUN 2019", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 12),),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Indra", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Pembayaran", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Text("-Rp500.000", style: TextStyle(color: Colors.red[400], fontSize: 14),),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Sky Parking", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Pembayaran", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text("-Rp4.500", style: TextStyle(color: Colors.red[400], fontSize: 14),),
                      Text("+Point 1.200", style: TextStyle(color: Colors.blue[400], fontSize: 14),)
                    ],
                  )
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Transfer from TOP UP", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Incoming Transfer", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Text("+Rp1.000.000", style: TextStyle(color: Colors.green[600], fontSize: 14),),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.black12,
          ),
          child: Text("25 JUN 2019", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 12),),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Indra", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Pembayaran", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Text("-Rp500.000", style: TextStyle(color: Colors.red[400], fontSize: 14),),
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Sky Parking", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Pembayaran", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text("-Rp4.500", style: TextStyle(color: Colors.red[400], fontSize: 14),),
                      Text("+Point 1.200", style: TextStyle(color: Colors.blue[400], fontSize: 14),)
                    ],
                  )
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 10, top: 0, bottom: 0, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: ListTile(
            title: Text("Transfer from TOP UP", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            contentPadding: EdgeInsets.only(top: 1),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("Incoming Transfer", style: TextStyle(color: Colors.grey, fontSize: 14),),
                ),
                Container(
                  child: Text("+Rp1.000.000", style: TextStyle(color: Colors.green[600], fontSize: 14),),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}