
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:barcode_scan/barcode_scan.dart';

// import '../component/label_below_icon.dart';
import 'topup.dart';
import 'service_all.dart';
import '../form_service/pln.dart';
import '../form_service/pulsa.dart';
import '../form_service/paket_data.dart';
import 'profile_id.dart';
import 'scan_qr.dart';

class Home extends StatelessWidget {

  Widget topup(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 10),
      color: Colors.blue.shade400,
      alignment: FractionalOffset.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("SALDO ANDA", style: TextStyle(color: Colors.white, fontSize: 16),),
                Text("Rp. 15.000", style: TextStyle(color: Colors.amber[600], fontSize: 28, fontWeight: FontWeight.bold),),
                Row(
                  children: <Widget>[
                    Text("Poin : ", style: TextStyle(color: Colors.white, fontSize: 16),),
                    Text("5.520", style: TextStyle(color: Colors.amber[600], fontSize: 16, fontWeight: FontWeight.bold))
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 20, right: 10, bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 0),
                  child: IconButton(
                    icon: Icon(Icons.refresh, color: Colors.white,),
                    onPressed: () {},
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 0),
                  child: FlatButton(
                    padding: EdgeInsets.all(15),
                    child: Text("TOP UP", style: TextStyle(color: Colors.white),),
                    color: Colors.blue[700],
                    onPressed: () {
                      Navigator.push(
                        context, 
                        MaterialPageRoute(
                          builder: (BuildContext context) {
                            return TopUp();
                          }
                        )
                      );
                    },
                  )
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget labelIcon(BuildContext context, iconColor, icon, textLabel, onPressed) {
    return InkWell(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            backgroundColor: iconColor,
            child: Icon(
              icon,
              size: 25.0,
              color: Colors.white,
              textDirection: TextDirection.rtl,
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Text(
            textLabel,
            textAlign: TextAlign.center,
            style: TextStyle(fontFamily: 'Raleway'),
          )
        ],
      ),
      onTap: onPressed
    );
  }

  Widget service(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Card(
        elevation: 2.0,
        child: Padding(
          padding: const EdgeInsets.only(left: 8, top: 40, right: 8),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        child: labelIcon(context, Colors.blue, Icons.flash_on, "PLN", () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (BuildContext context) {
                              return Pln();
                            })
                          );
                        }),
                      ),
                      Container(
                        child: labelIcon(context, Colors.orange, Icons.phone_android, "Pulsa", () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (BuildContext context) {
                              return Pulsa();
                            })
                          );
                        }),
                      ),
                      Container(
                        child: labelIcon(context, Colors.purple, Icons.wifi, "Paket\nData", () {
                          Navigator.push(
                            context, 
                            MaterialPageRoute(builder: (BuildContext context) {
                              return PaketData();
                            })
                          );
                        })
                      ),
                      Container(
                        child: labelIcon(context, Colors.indigo, Icons.phonelink_ring, "Pascabayar", () {}),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        child: labelIcon(context, Colors.red, Icons.local_hospital, "BPJS", () {}),
                      ),
                      Container(
                        child: labelIcon(context, Colors.teal, FontAwesomeIcons.stream, "Telkom", () {}),
                      ),
                      Container(
                        child: labelIcon(context, Colors.lime, FontAwesomeIcons.bookReader, "Asuransi", () {}),
                      ),
                      Container(
                        child: labelIcon(
                          context, 
                          Colors.black38, 
                          Icons.more_horiz, 
                          "See All", 
                          (){ 
                            Navigator.push(
                              context, 
                              MaterialPageRoute(builder: (BuildContext context) {
                                return ServiceAll();
                              })
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Size deviceSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              topup(context),
              SizedBox(
                height: deviceSize.height * 0.02,
              ),
              service(context)
            ],
          ),
          Align(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              margin: EdgeInsets.symmetric(horizontal: 30),
              height: 90,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    spreadRadius: 0,
                    blurRadius: 5,
                    offset: Offset(0, 6)
                  )
                ]
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: InkWell(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.input, size: 35,),
                          Text("Transfer", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),)
                        ],
                      ),
                      onTap: (){},
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.fullscreen, size: 35,),
                          Text("Scan QR", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16))
                        ],
                      ),
                      onTap: (){
                        Navigator.push(
                          context, 
                          MaterialPageRoute(builder: (BuildContext context) {
                            return ScanQR();
                          })
                        );
                      },
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.account_box, size: 35,),
                          Text("MOBI ID", style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16))
                        ],
                      ),
                      onTap: (){
                        Navigator.push(
                          context, 
                          MaterialPageRoute(builder: (BuildContext context) {
                            return ProfileId();
                          })
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
            alignment: Alignment(0, -0.50),
          )
        ],
      )
    );
  }


}