// import 'dart:io';
// import 'dart:typed_data';
// import 'dart:ui' as prefix0;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
// import 'package:path_provider/path_provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
// import 'package:flutter/services.dart';

class ProfileId extends StatefulWidget {
  ProfileId({Key key}) : super(key: key);

  _ProfileIdState createState() => _ProfileIdState();
}

class _ProfileIdState extends State<ProfileId> {

  GlobalKey globalKey = new GlobalKey();
  String _dataString = "Mobi123";
//  String _inputErrorText;

  // Future<void> _captureAndSharePng() async {
  //   try {
  //     RenderRepaintBoundary boundary = globalKey.currentContext.findRenderObject();
  //     var image = await boundary.toImage();
  //     ByteData byteData = await image.toByteData(format: prefix0.ImageByteFormat.png);
  //     Uint8List pngBytes = byteData.buffer.asUint8List();

  //     final tempDir = await getTemporaryDirectory();
  //     final file = await new File('${tempDir.path}/image.png').create();
  //     await file.writeAsBytes(pngBytes);

  //     final channel = const MethodChannel('channel:me.alfian.share/share');
  //     channel.invokeMethod('shareFile', 'image.png');

  //   } catch(e) {
  //     print(e.toString());
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MOBI id"),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: Colors.black12, style: BorderStyle.solid, width: 1))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26, style: BorderStyle.solid, width: 4),
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.black12
                  ),
                  padding: EdgeInsets.all(3),
                  child: Icon(Icons.account_circle, size: 40, color: Colors.white),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Muhammad Indra Permana", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),),
                      SizedBox(
                        height: 5,
                      ),
                      Text("MOBI Primare", style: TextStyle(color: Colors.blue, fontSize: 16, fontWeight: FontWeight.w500),)
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black12, style: BorderStyle.solid, width: 1),
              borderRadius: BorderRadius.circular(5)
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Text("QR CODE", style: TextStyle(fontWeight: FontWeight.bold),),
                    ),
                    Icon(Icons.info, color: Colors.blueGrey,)
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Center(
                    child: RepaintBoundary(
                      key: globalKey,
                      child: QrImage(
                        data: _dataString,
                        // size: 0.5 * bodyHeight,
                        size: 200.0,
                        onError: (ex) {
                          print("[QR] ERROR - $ex");
//                          setState((){
//                            _inputErrorText = "Error! Maybe your input value is too long?";
//                          });
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}