import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../form_service/pln.dart';
import '../form_service/pulsa.dart';

class ServiceAll extends StatelessWidget {

  Widget labelIcon(BuildContext context, iconColor, icon, textLabel, onPressed) => InkWell(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        CircleAvatar(
          backgroundColor: iconColor,
          child: Icon(
            icon,
            size: 25.0,
            color: Colors.white,
          ),
        ),
        SizedBox(
          height: 5.0,
        ),
        Text(
          textLabel,
          textAlign: TextAlign.center,
          style: TextStyle(fontFamily: 'Raleway'),
        )
      ],
    ),
    onTap: onPressed
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Service"),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          child: labelIcon(context, Colors.blue, Icons.flash_on, "PLN", () {
                            Navigator.push(
                              context, 
                              MaterialPageRoute(builder: (BuildContext context) {
                                return Pln();
                              })
                            );
                          }),
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.orange, Icons.phone_android, "Pulsa", () {
                            Navigator.push(
                              context, 
                              MaterialPageRoute(builder: (BuildContext context) {
                                return Pulsa();
                              })
                            );
                          }),
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.purple, Icons.wifi, "Paket Data", () {})
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.indigo, Icons.phonelink_ring, "Pascabayar", () {}),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          child: labelIcon(context, Colors.red, Icons.local_hospital, "BPJS", () {}),
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.teal, FontAwesomeIcons.stream, "Telkom", () {}),
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.lime, FontAwesomeIcons.bookReader, "Asuransi", () {}),
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.indigo, FontAwesomeIcons.water, "PAM", () {}),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                          child: labelIcon(context, Colors.blue, FontAwesomeIcons.solidUser, "Friends", () {})
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.orange, FontAwesomeIcons.userFriends, "Groups", () {}),
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.purple, FontAwesomeIcons.mapMarkerAlt, "Nearby", () {}),
                        ),
                        Expanded(
                          child: labelIcon(context, Colors.indigo, FontAwesomeIcons.locationArrow, "Moment", () {}),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}