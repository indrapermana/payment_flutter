import 'package:flutter/material.dart';

class TopUp extends StatelessWidget {
  const TopUp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TOP UP"),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 30, top: 30, right: 30),
        child: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.grey)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Top Up Saldo Ke", style: TextStyle(color: Colors.grey, fontSize: 12),),
                  Text("MOBI Cash")
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey[700],
                  width: 1,
                  style: BorderStyle.solid
                ),
                borderRadius: BorderRadius.circular(5),
                color: Colors.grey[200],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("SALDO MOBI CASH", style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),),
                  Text("Rp15.000", style: TextStyle(fontWeight: FontWeight.bold),)
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: Text("Maks. Saldo MOBI Cash Rp10.000.000"),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Top up makin mudah dengan motode berikut:"),
                  InkWell(
                    child: Card(
                      margin: EdgeInsets.only(top: 10),
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.payment),
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text("Kartu Debit", style: TextStyle(),),
                            )
                            // Icon(Icons.arrow_forward_ios, size: 20, )
                          ],
                        ),
                      )
                    ),
                    onTap: () {},
                  ),
                  InkWell(
                    child: Card(
                      margin: EdgeInsets.only(top: 10),
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.account_balance),
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text("ATM", style: TextStyle(),),
                            )
                            // Icon(Icons.arrow_forward_ios, size: 20, )
                          ],
                        ),
                      )
                    ),
                    onTap: () {},
                  ),
                  InkWell(
                    child: Card(
                      margin: EdgeInsets.only(top: 10),
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.phone_iphone),
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text("Internet / Mobile Banking", style: TextStyle(),),
                            )
                            // Icon(Icons.arrow_forward_ios, size: 20, )
                          ],
                        ),
                      )
                    ),
                    onTap: () {},
                  ),
                  InkWell(
                    child: Card(
                      margin: EdgeInsets.only(top: 10),
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.store),
                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: Text("Merchant", style: TextStyle(),),
                            )
                            // Icon(Icons.arrow_forward_ios, size: 20, )
                          ],
                        ),
                      )
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}