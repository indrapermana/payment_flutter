import 'package:flutter/material.dart';

import 'home/home.dart';
import 'wallet/wallet.dart';
import 'history/history.dart';
import 'profile/profile.dart';
import 'setting/settings.dart';
import 'notification/notif.dart';

void main(){
  runApp(MaterialApp(
    title: "LayoutUI",
    home: LayoutUI(),
  ));
}

class LayoutUI extends StatefulWidget {
  @override
  _LayoutUIState createState() => _LayoutUIState();
}

class _LayoutUIState extends State<LayoutUI> with SingleTickerProviderStateMixin {

  TabController controller;

  @override
  void initState() { 
    controller = TabController(vsync: this, length: 4);
    super.initState();
  }

  @override
  void dispose() { 
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MOBI", style: TextStyle(fontWeight: FontWeight.bold),),
        bottom: TabBar(
          controller: controller,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.home)),
            Tab(icon: Icon(Icons.account_balance_wallet)),
            Tab(icon: Icon(Icons.history)),
            // Tab(icon: Icon(Icons.person_pin), text: "Profile"),
            InkWell(
              child: Tab(icon: Icon(Icons.account_circle, )),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (BuildContext context){
                    return Profile();
                  }) 
                );
              }
            )
            // GestureDetector(
            //   child: Tab(icon: Icon(Icons.person_pin)),
            //   onTap: (){
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(builder: (BuildContext context){
            //         return Profile();
            //       }) 
            //     );
            //   },
            // )
          ],
        ),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications, color: Colors.white,),
            onPressed: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (BuildContext context){
                  return Notif();
                })
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.settings, color: Colors.white),
            onPressed: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (BuildContext context){
                  return Settings();
                })
              );
            },
          )
        ],
      ),
      body: TabBarView(
        controller: controller,
        children: <Widget>[
          Home(),
          Wallet(),
          History(),
          Profile()
          // InkWell(
          //   onTap: (){
          //     Navigator.push(
          //       context,
          //       MaterialPageRoute(builder: (BuildContext context){
          //         return Profile();
          //       }) 
          //     );
          //   },
          // )
        ],
      ),
      // bottomNavigationBar: new Material(
      //   color: Colors.blue,
      //   child: new TabBar(
      //     controller: controller,
      //     tabs: <Widget>[
      //       new Tab(icon: new Icon(Icons.home),),
      //       new Tab(icon: new Icon(Icons.account_balance_wallet),),
      //       new Tab(icon: new Icon(Icons.history),),
      //       // new Tab(icon: new Icon(Icons.account_circle),),
      //     ],
      //   ),
      // ),
    );
  }
}