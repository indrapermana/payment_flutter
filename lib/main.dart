import 'package:flutter/material.dart';

// import 'login.dart';
import 'layout_ui.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LayoutUI(),
      // routes: <String,WidgetBuilder>{
      //   '/home': (BuildContext context)=> new Login(),
      //   '/MemberPage': (BuildContext context)=> new Home(),
      // },
    );
  }
}