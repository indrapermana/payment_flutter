import 'package:flutter/material.dart';

import 'message_detail.dart';

class Message extends StatelessWidget {
  const Message({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.black12,
          ),
          child: Text("25 JUN 2019", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 12),),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: InkWell(
            child: Text("Lawan tanggal tua dengan Cashback 30% di ratusan merchant favorit!"),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
                return MessageDetail();
              }));
            },
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: InkWell(
            child: Text("Lawan tanggal tua dengan Cashback 30% di ratusan merchant favorit!"),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
                return MessageDetail();
              }));
            },
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.black12,
          ),
          child: Text("25 JUN 2019", style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold, fontSize: 12),),
        ),
        Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: InkWell(
            child: Text("Lawan tanggal tua dengan Cashback 30% di ratusan merchant favorit!"),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
                return MessageDetail();
              }));
            },
          ),
        )
      ],
    );
  }
}