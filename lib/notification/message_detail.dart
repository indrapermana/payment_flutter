import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
// import 'package:flutter_html_view/flutter_html_view.dart';

// import '../component/doscription_text_html.dart';

class MessageDetail extends StatelessWidget {
  // const MessageDetail({Key key}) : super(key: key);

//  final double _appBarHeight = 256.0;

  final String description = "<strong>Detail</stong><br><br>Belanja dari toko ke toko, makan dari resto ke resto, let's go dapatkan Cashback 30% di ratusan merchant dan ribuan outlet favorit di Indonesia, seperti: <br><br> - <br><br><u><strong>Food & Beverages</strong></u><br><br><ul><li>10 Ten</li><li>Aneka Citra Snack</li><li>Angel In Us Coffee</li><li>Auntie Anne's</li><li>Ayam Goreng Karawaci</li><li>Ayam Keprabon Express</li><li>Bakerzin</li><li>Bakmi GM</li><li>Bakmi Gocit</li><li>Bakso Atom</li><li>Bakso Lapangan Tembak</li><li>Ban Ban</li><li>Baskin Robbins</li><li>Baso A Fung</li><li>Baso by Mister Baso</li><li>Baso Malang Karapitan</li><li>Dan Jajanan Lainnya</li></ul>";

  final List datas = ["10 Ten", "Aneka Citra Snack", "Angel In Us Coffee", "Auntie Anne's", "Ayam Goreng Karawaci", "Ayam Keprabon Express", "Bakerzin", "Bakmi GM", "Bakmi Gocit", "Bakso Atom", "Bakso Lapangan Tembak", "Ban Ban", "Baskin Robbins", "Baso A Fung", "Baso by Mister Baso", "Baso Malang Karapitan", "Beau", "Bengawan Solo Coffee", "BreadLife", "Cable Car Cafe", "Calais", "Caribou Coffee", "Dan Jajan Lainnya"];

  Widget descriptions(){
    final _markDownData = datas.map((x) => "- $x\n").reduce((x, y) => "$x$y");
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text("Belanja dari toko ke toko, makan dari resto ke resto, let's go dapatkan Cashback 30% di ratusan merchant dan ribuan outlet favorit di Indonesia, seperti:"),
          SizedBox(
            height: 30,
          ),
          Text("Food & Beverages", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(
            height: 20,
          ),
          Markdown(data: _markDownData),

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double cWidth = MediaQuery.of(context).size.width*0.8;
    return Scaffold(
      appBar: AppBar(
        title: Text("Maraton belanja se-Indoensia!"),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          Text("Maraton belanja Se-Indonesia!", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
          SizedBox(
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.blue[700],
              border: Border.all(color: Colors.blue, width: 1, style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(20)
            ),
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.symmetric(horizontal: 100),
            child: Text("Hingga 29 Sep 2019", style: TextStyle(color: Colors.white, fontSize: 12),),
          ),
          SizedBox(
            height: 10,
          ),
          Image.asset(
            "assets/images/iklan1.jpg",
            fit: BoxFit.cover,
            height: 200,
          ),
          // DescriptionTextWiget(text: description)
          // HtmlView(data: description,)
          SizedBox(
            height: 10,
          ),
          Text("Belanja dari toko ke toko, makan dari resto ke resto, let's go dapatkan Cashback 30% di ratusan merchant dan ribuan outlet favorit di Indonesia, seperti:"),
          SizedBox(
            height: 30,
          ),
          Text("Food & Beverages", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(
            height: 20,
          ),
          Text("- 10 Ten"),
          Text("- Aneka Citra Snack"),
          Text("- Angel In Us Coffee"),
          Text("- Auntie Anne's"),
          Text("- Ayam Goreng Karawaci"),
          Text("- Ayam Keprabon Express"),
          Text("- Bakerzin"),
          Text("- Bakmi GM"),
          Text("- Bakmi Gocit"),
          Text("- Bakso Atom"),
          Text("- Bakso Lapangan Tembak"),
          Text("- Ban Ban"),
          Text("- Baskin Robbins"),
          Text("- Baso A Fung"),
          Text("- Baso by Mister Baso"),
          Text("- Baso Malang Karapitan"),
          Text("- Beau"),
          Text("- Bengawan Solo Coffee"),
          Text("- BreadLife"),
          Text("- Cable Car Cafe"),
          Text("- Calais"),
          Text("- Caribou Coffee"),
          Text("- Dan Lain-lain"),
          SizedBox(
            height: 30,
          ),
          Text("Kategori lainnya", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(
            height: 20,
          ),
          Text("- Century"),
          Text("- Gramedia"),
          Text("- Inul Vizta"),
          Text("- JYSK"),
          Text("- Meiso Reflexology"),
          Text("- Miniso"),
          Text("- MOR"),
          Text("- Polo"),
          SizedBox(
            height: 30,
          ),
          Text("Syarat & Ketentuan", style: TextStyle(fontWeight: FontWeight.bold)),
          SizedBox(
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("1.  "),
              Container(
                width: cWidth,
                child: Text("Setiap transaksi min. Rp 10.000 menggunakan MOBI Cash, berhak mendapatkan Cashback 30% dalam bentuk MOBI Points (1 MOBI Point = Rp 1)"),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("2.  "),
              Container(
                width: cWidth,
                child: Text("Maks. Cashback yang diberikan hingga 20.000 MOBI Points /struk/transaksi dan hingga 40.000 MOBI Points/bulan/merchant (bukan per outlet)."),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("3.  "),
              Container(
                width: cWidth,
                child: Text("MOBI berhak membatalkan MOBI Points yang telah diberikan apabila ditemukan kecurangan dalam pelaksanaan promo ini."),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("4.  "),
              Container(
                width: cWidth,
                child: Text("Jika terjadi kegagalan transaksi MOBI karena alasan teknis apa pun (baik EDC, jaringan koneksi atau aplikasi) maka kegagalan tersebut tidak menjadi tanggung jawab merchant. Konsumen wajib melakukan pelunasan pembayaran dan bisa mengubungi CS MOBI."),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("5.  "),
              Container(
                width: cWidth,
                child: Text("Syarat dan ketentuan promo ini dapat berubah sewaktu-waktu."),
              )
            ],
          ),
        ],
      ),
    );
  }
}