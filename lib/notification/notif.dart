import 'package:flutter/material.dart';

import 'notifications.dart';
import 'message.dart';

class Notif extends StatefulWidget {
  Notif({Key key}) : super(key: key);

  _NotifState createState() => _NotifState();
}

class _NotifState extends State<Notif> with SingleTickerProviderStateMixin {

  TabController controllerNotif;

  @override
  void initState() { 
    controllerNotif = TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() { 
    controllerNotif.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("NOTIFICATIONS"),
        bottom: TabBar(
          controller: controllerNotif,
          tabs: <Widget>[
            Tab(text: "Notification",),
            Tab(text: "Pesan",)
          ],
        ),
      ),
      body: TabBarView(
        controller: controllerNotif,
        children: <Widget>[
          Notifications(),
          Message()
        ],
      ),
    );
  }
}