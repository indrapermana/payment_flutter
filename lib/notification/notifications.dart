import 'package:flutter/material.dart';

import 'notif_detail.dart';

class Notifications extends StatelessWidget {
  const Notifications({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final double cWidth = MediaQuery.of(context).size.width*0.7;
    
    final ThemeData theme = Theme.of(context);

    return ListsNotif(context, theme, cWidth);
  }
}

 Widget ListsNotif(BuildContext context, ThemeData theme, width){
  List<Widget> list = new List<Widget>();
  for(var i=0; i<=10; i++){
    list.add(Container(
          padding: EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          decoration: BoxDecoration(
            color: theme.canvasColor,
            border: Border(bottom: BorderSide(color: theme.dividerColor)),
          ),
          child: InkWell(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.blue,
                  child: Icon(
                    Icons.photo_filter, 
                    size: 30, 
                    color: Colors.white,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10),
                  width: width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Indra Permana Berhasil Melakukan Pembayaran", style: TextStyle(fontSize: 14),),
                      SizedBox(
                        height: 5,
                      ),
                      Text("1 hour(s) ago", style: TextStyle(color: Colors.grey, fontSize: 12),)
                    ],
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.push(
                context, 
                MaterialPageRoute(
                  builder: (BuildContext context){
                    return NotifDetail();
                  }
                )
              );
            },
          ),
        )
      );
  }
  return ListView(children: list,);
}