//import 'dart:io';
//
//import 'package:flutter/material.dart';
//import 'package:pdf/pdf.dart';
//import 'package:printing/printing.dart';
//import 'package:flutter_share/flutter_share.dart';
//import 'package:path_provider/path_provider.dart';
//
//class PrintDoc extends StatelessWidget {
//
//  Future<PdfDocument> generateDocument() async {
//    final pdf = new PdfDocument();
//    final page = new PdfPage(pdf, pageFormat: PdfPageFormat(216.0, 384));
//    final top = page.pageFormat.height;
//    final graphics = page.getGraphics();
//    final font = PdfFont.courier(pdf);
//
//    graphics.setColor(PdfColor(0.3, 0.3, 0.3));
//    String text = "We Can only use ASCII characters";
//    graphics.drawString(font, 12.0, text, 1.0 * PdfPageFormat.mm, top-10*PdfPageFormat.mm);
//
//    await _localFile.then((File file){
//      print("Saving local file");
//      file.writeAsBytesSync(pdf.save());
//    });
//
//    return null;
//  }
//
//  void _sharePDF() {
//    print("Print ...");
//    generateDocument().then((pdf) {
//      _localPath.then((String path){
//        FlutterShare.share(fileUrl: "$path/pdf.pdf");
//      });
//    });
//
//  }
//
//  Future<File> get _localFile async {
//    final path = await _localPath;
//    return File('$path/pdf.pdf');
//  }
//
//  Future<String> get _localPath async {
//    final directory = await getApplicationDocumentsDirectory();
//    return directory.path;
//  }
//
//  Future<void> _printHtml() async{
//    print("Print ...");
//    await Printing.layoutPdf(
//        onLayout: (PdfPageFormat format) async => await Printing.convertHtml(
//          format: format,
//          html: '<html><body><p>Hello!</p></body></html>',
//        )
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: new AppBar(
//        title: const Text('Printing example'),
//      ),
//      body: new Center(
//        child: new Column(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//          children: <Widget>[
//            new RaisedButton(
//                child: new Text('Print Document'),
//                onPressed: _sharePDF
//            ),
//            new RaisedButton(
//                child: new Text('Print Document 1'),
//                onPressed: _printHtml
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}
