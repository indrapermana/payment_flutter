import 'package:flutter/material.dart';

import '../login.dart';

class Settings extends StatelessWidget {
  const Settings({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SETTINGS"),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Akun", style: TextStyle(color: Colors.blue[600], fontSize: 18),),
                FlatButton(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text("Edit Profile"),
                  onPressed: () {},
                ),
                FlatButton(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text("Kode Promo"),
                  onPressed: () {},
                ),
                SizedBox(
                  height: 20,
                ),
                Text("Keamanan", style: TextStyle(color: Colors.blue[600], fontSize: 18),),
                FlatButton(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text("Ubah Security Code"),
                  onPressed: () {},
                ),
                SizedBox(
                  height: 20,
                ),
                Text("Tentang", style: TextStyle(color: Colors.blue[600], fontSize: 18),),
                FlatButton(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text("Tentang MOBI"),
                  onPressed: () {},
                ),
                FlatButton(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text("Kabijakan Privasi"),
                  onPressed: () {},
                ),
                FlatButton(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text("Pusat Bantuan"),
                  onPressed: () {},
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: OutlineButton(
                    child: Text("SIGN OUT", style: TextStyle(color: Colors.blue[600], fontWeight: FontWeight.bold, fontSize: 16),),
                    color: Colors.blue[600],
                    padding: EdgeInsets.symmetric(horizontal: 50),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    borderSide: BorderSide(
                      color: Colors.blue[600],
                      width: 1.5,
                      style: BorderStyle.solid
                    ),
                    onPressed: () {
                      Navigator.pushReplacement(
                        context, 
                        MaterialPageRoute(builder: (BuildContext context){
                          return Login();
                        })
                      );
                      // Navigator.push(
                      //   context, 
                      //   MaterialPageRoute(builder: (BuildContext context){
                      //     return Login();
                      //   })
                      // );
                    },
                  ),
                )
              ],
            ),
        ],
      )
    );
  }
}