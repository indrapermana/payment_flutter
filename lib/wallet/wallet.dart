import 'package:flutter/material.dart';

class Wallet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 30),
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/mountains.jpeg"),
              fit: BoxFit.fill,
              matchTextDirection: true
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black, 
                blurRadius: 10, 
                offset: Offset(0.0, 5.0)
              )
            ],
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("MOBI", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),),
              SizedBox(
                height: 30,
              ),
              Text("0000 - 0000 - 0000 - 0000", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16)),
              SizedBox(
                height: 20,
              ),
              Text("MUHAMMAD INDRA PERMANA", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16)),
            ],
          ),
        ),
        Text("MOBI", style: TextStyle(color: Colors.blue[600], fontSize: 18, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
        Text("5 Ketungan", style: TextStyle(fontWeight: FontWeight.bold),),
        SizedBox(
          height: 10,
        ),
        Text("- Point Perlipat"),
        Text("Kumpulan poin tiap belanja di berbagai merchant."),
        SizedBox(
          height: 10,
        ),
        Text("- Promo Memikat"),
        Text('Temukan berbagai penawaran menarik di dalam "Deals" dan dapatkan keuntungan lainnya dari merchant rekan MOBI.'),
        SizedBox(
          height: 10,
        ),
        Text("- Merchant  Hebat di Banyak Tempat"),
        Text("Gunakan MOBI Points di berbagai merchant rekan MOBI."),
        SizedBox(
          height: 10,
        ),
        Text("- Atur Keuangan dengan Tepat"),
        Text("Kelola & Monitor pengeluaran Anda mengguanakan MOBI."),
        SizedBox(
          height: 10,
        ),
        Text("- Pembayaran Lebih Cepat"),
        Text("Rasakan kemudahan bertransaksi dengan menggunakan MOBI."),
        
      ],
    );
  }
}